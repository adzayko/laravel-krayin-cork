console.log("mcs-peyd.js");

import Vue from 'vue';
import '@/assets/sass/app.scss';
import Header from './components/layout/header.vue';
import Sidebar from './components/layout/sidebar.vue';
import SidebarItem from './components/layout/sidebar-item.vue';
import Footer from './components/layout/footer.vue';
import appSettings from './components/app-settings.vue';
import InventoryTable from './views/mcs/inventory_table.vue';
import InvoiceList from './views/apps/invoice/list.vue';
import PendingInventoryForm from './views/mcs/pending-inventory-form.vue';

Vue.component('v-header', Header);
Vue.component('v-sidebar', Sidebar);
Vue.component('v-sidebar-item', SidebarItem);
Vue.component('v-footer', Footer);
Vue.component('app-settings', appSettings);
Vue.component('inventory-table', InventoryTable);
Vue.component('invoice-list', InvoiceList);
Vue.component('pending-inventory-form', PendingInventoryForm);