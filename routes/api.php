<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('peyd_points', \MCS\Peyd\Http\Controllers\API\PointsController::class)->names([
    'index' => 'api.peyd_points.index',
    'store' => 'api.peyd_points.store',
    'show' => 'api.peyd_points.show',
    'update' => 'api.peyd_points.update',
    'destroy' => 'api.peyd_points.destroy',
]);