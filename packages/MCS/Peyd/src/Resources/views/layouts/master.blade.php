<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <title>@yield('page_title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="57x57"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16"
        href="{{ asset('vendor/webkul/admin/assets/images/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('vendor/webkul/admin/assets/images/favicon/manifest.json') }}">

    {{-- <link rel="stylesheet" href="{{ asset('vendor/webkul/ui/assets/css/ui.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/webkul/admin/assets/css/admin.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

    @yield('head')

    @yield('css')
    @stack('css')

    {!! view_render_event('admin.layout.head') !!}

</head>

<body style="scroll-behavior: smooth;" @if (app()->getLocale() == 'ar') class="rtl" @endif>
    {!! view_render_event('admin.layout.body.before') !!}

    <div id="app">

        <div>
            <!--  BEGIN NAVBAR  -->
            <v-header></v-header>
            <!--  END NAVBAR  -->

            <!--  BEGIN MAIN CONTAINER  -->
            <div class="main-container" id="container"
                :class="[!$store.state.is_show_sidebar ? 'sidebar-closed sbar-open' : '', $store.state
                    .menu_style === 'collapsible-vertical' ? 'collapsible-vertical-mobile' : ''
                ]">
                <!--  BEGIN OVERLAY  -->
                <div class="overlay" :class="{ show: !$store.state.is_show_sidebar }"
                    @click="$store.commit('toggleSideBar', !$store.state.is_show_sidebar)"></div>
                <div class="search-overlay" :class="{ show: $store.state.is_show_search }"
                    @click="$store.commit('toggleSearch', !$store.state.is_show_search)"></div>
                <!-- END OVERLAY -->

                <!--  BEGIN SIDEBAR  -->
                <v-sidebar>
                    <v-sidebar-item link="{{ route("admin.peyd.index") }}" label="Dashboard"></v-sidebar-item>
                    <v-sidebar-item link="{{ route("admin.peyd.inventory.index") }}" label="Inventory"></v-sidebar-item>
                </v-sidebar>
                <!--  END SIDEBAR  -->

                <!--  BEGIN CONTENT AREA  -->
                <div id="content" class="main-content">
                    
                    @yield('content-wrapper')

                    <!-- BEGIN FOOTER -->
                    <v-footer></v-footer>
                    <!-- END FOOTER -->
                </div>
                <!--  END CONTENT AREA  -->

                <!-- BEGIN APP SETTING LAUNCHER -->
                <app-settings></app-settings>
                <!-- END APP SETTING LAUNCHER -->
            </div>
        </div>

    </div>

    {{-- <script type="text/javascript" src="{{ asset('vendor/webkul/admin/assets/js/admin.js') }}"></script> --}}
    {{-- <script type="text/javascript" src="{{ asset('vendor/webkul/ui/assets/js/ui.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    <script src="{{ asset(mix('js/main.js')) }}"></script>

    @stack('scripts')

    {!! view_render_event('admin.layout.body.after') !!}

    <div class="modal-overlay"></div>
</body>

</html>
