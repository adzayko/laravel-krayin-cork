@extends('peyd::layouts.master')

@push('css')
    <!-- ADZ ADZ ADZ -->
    <style>
        #peyd-app {
            min-height: 90vh;
        }
    </style>
@endpush

@section('content-wrapper')

    <div class="page-content">

        <div id="peyd-app" class="peyd-content">
            @include('peyd::layouts.breadcrumbs', [
                'parent' => 'Inventory',
                'current' => 'Pending'
            ])

            <div class="col-md-12">
                @yield('peyd-content')
            </div>
        </div>

    </div>

@stop
