@extends('peyd::layouts/main')

@section('peyd-header')

    <div class="page-title">
        <h4>Home</h4>
    </div>

    <div class="page-actions">
    </div>

@stop

@section('peyd-content')

    <invoice-list></invoice-list>

@stop
