@extends('peyd::layouts.main')

@push('page_title')
    Pending Inventory
@endpush

@section('peyd-breadcrumbs')
    Create Pending
@stop

@section('peyd-content')

    <div class="container">
        <div class="mb-3 mt-3">
    
            <pending-inventory-form />
            
        </div>
    </div>

@stop
