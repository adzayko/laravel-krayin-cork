@extends('peyd::layouts.main')

@section('peyd-header')

    <div class="page-title">
        <h1>
            Inventory
        </h1>
    </div>

    <div class="page-actions">
    </div>

@stop

@push('css')
    <style>
        #inventory-tabs-content {
            padding: 18px;
            background-color: #e0e0e0;
            min-height: 800px;
        }
    </style>
@endpush

@section('peyd-content')

    <b-tabs nav-class="mb-3 mt-3">
        <b-tab title="Pending Inventory" active>
            @include('peyd::inventory.includes.pending')
        </b-tab>
        <b-tab title="Live Inventory">
            @include('peyd::inventory.includes.live')
        </b-tab>
        <b-tab title="Accounting">
            @include('peyd::inventory.includes.accounting')
        </b-tab>
        <b-tab title="Closed">
            @include('peyd::inventory.includes.closed')
        </b-tab>
    </b-tabs>

@stop
