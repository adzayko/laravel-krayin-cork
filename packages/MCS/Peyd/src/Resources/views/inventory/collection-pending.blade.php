@extends('peyd::layouts.main')

@section('peyd-header')

    <div class="page-title">
        <h3>Pending Inventory (INVENTORY ID) (ICON) (NAME)</h3>
    </div>

    <div class="page-actions">
    </div>

@stop

@section('peyd-content')

    <section id="collection-pending">
        <div>
            <h4>Collection Pending</h4>
        </div>

        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Account ID</th>
                        <th>Account Name</th>
                        <th>Account Type</th>
                        <th>Account Total</th>
                        <th>Account Status</th>
                        <th>Account Created</th>
                        <th>Account Updated</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pendingInventory as $inventory)
                        <tr>
                            <td>{{ $inventory->id }}</td>
                            <td>{{ $inventory->name }}</td>
                            <td>{{ $inventory->type }}</td>
                            <td>{{ $inventory->account_total }}</td>
                            <td>{{ $inventory->status }}</td>
                            <td>{{ $inventory->created_at }}</td>
                            <td>{{ $inventory->updated_at }}</td>
                            <td>
                                <a href="{{ route('admin.peyd.pending-inventory.view', $inventory->id) }}" class="btn btn-primary btn-sm">View</a>
                                <a href="{{ route('admin.peyd.pending-inventory.edit', $inventory->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                <a href="{{ route('admin.peyd.pending-inventory.destroy', $inventory->id) }}" class="btn btn-danger btn-sm">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <a href="{{ route('admin.peyd.pending-inventory.create') }}" class="btn btn-primary btn-md">Add New Record</a>
    </section>

@stop
