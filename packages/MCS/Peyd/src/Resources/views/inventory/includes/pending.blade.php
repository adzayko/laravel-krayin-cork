@push('page_title')
    Pending Inventory
@endpush

@section('peyd-breadcrumbs')
    Pending
@stop

<section id="peyd-collection-pending">
    <div class="mb-3 d-flex justify-content-between"> <!-- Add these classes -->
        <h4>Pending Inventory</h4>
        @if (bouncer()->hasPermission('quotes.create'))
            <a href="{{ route('admin.peyd.pending-inventory.create') }}" class="btn btn-primary">Create</a>
        @endif
    </div>

    <div>
        <inventory-table src="{{ route('api.peyd_points.index') }}"></inventory-table>
    </div>

</section>
