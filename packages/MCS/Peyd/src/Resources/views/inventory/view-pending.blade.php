@extends('peyd::layouts.main')

@push('page_title')
    Pending Inventory
@endpush

@push('css')
    <style>
        #pending-inventory-form input {
            margin-bottom: 3px;
            margin-top: 3px;
        }

        .peyd-cards {
            margin-bottom: 12px;
        }
    </style>
@endpush

@section('peyd-content')
    <div class="container">
        <div class="mb-3 mt-3">
            <pending-inventory-form json-src="{{ json_encode($points) }}" api-src="{{ route('api.peyd_points.update', ['peyd_point' => $points->id]) }}" />
        </div>
    </div>
@stop
