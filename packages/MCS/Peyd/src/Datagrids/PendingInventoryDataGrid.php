<?php

namespace MCS\Peyd\Datagrids;

use Illuminate\Support\Facades\DB;
use Webkul\UI\DataGrid\DataGrid;

// ['id', 'type', 'total_bought', 'buy_rate', 'sales_agent', 'p_pwp_pwu, 'can_use_extra', 'account_total', 'created_at', 'updated_at']
class PendingInventoryDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc';

    /**
     * Prepare query builder.
     *
     * @return void
     */
    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('peyd_points')
            ->join('peyd_points_meta', 'peyd_points.id', '=', 'peyd_points_meta.peyd_point_id');

        // Add filter for all columns
        $this->addFilter('id', 'peyd_points.id');
        $this->addFilter('type', 'peyd_points.type');
        $this->addFilter('total_bought', 'peyd_points.total_bought');
        $this->addFilter('buy_rate', 'peyd_points.buy_rate');
        $this->addFilter('sales_agent', 'peyd_points.sales_agent');
        $this->addFilter('p_pwp_pwu', 'peyd_points.p_pwp_pwu');
        $this->addFilter('can_use_extra', 'peyd_points.can_use_extra');
        $this->addFilter('account_total', 'peyd_points.account_total');
        $this->addFilter('created_at', 'peyd_points.created_at');
        $this->addFilter('updated_at', 'peyd_points.updated_at');

        $this->setQueryBuilder($queryBuilder);
    }

    /**
     * Add columns.
     *
     * @return void
     */
    public function addColumns()
    {
        $this->addColumn([
            'index'    => 'id',
            'label'    => 'ID',
            'type'     => 'number',
            'sortable' => true,
        ]);

        $this->addColumn([
            'index'    => 'type',
            'label'    => 'Type',
            'type'     => 'string',
            'sortable' => true,
        ]);

        $this->addColumn([
            'index'    => 'total_bought',
            'label'    => 'Total Bought',
            'type'     => 'number',
            'sortable' => true,
        ]);

        $this->addColumn([
            'index'    => 'buy_rate',
            'label'    => 'Buy Rate',
            'type'     => 'number',
            'sortable' => true,
        ]);

        $this->addColumn([
            'index'    => 'sales_agent',
            'label'    => 'Sales Agent',
            'type'     => 'string',
            'sortable' => true,
        ]);

        $this->addColumn([
            'index'    => 'p_pwp_pwu',
            'label'    => 'P PWP PWU',
            'type'     => 'string',
            'sortable' => true,
        ]);

        $this->addColumn([
            'index'    => 'can_use_extra',
            'label'    => 'Can Use Extra',
            'type'     => 'string',
            'sortable' => true,
        ]);

        $this->addColumn([
            'index'    => 'account_total',
            'label'    => 'Account Total',
            'type'     => 'number',
            'sortable' => true,
        ]);

        $this->addColumn([
            'index'    => 'created_at',
            'label'    => 'Created At',
            'type'     => 'datetime',
            'sortable' => true,
        ]);

        $this->addColumn([
            'index'    => 'updated_at',
            'label'    => 'Updated At',
            'type'     => 'datetime',
            'sortable' => true,
        ]);
    }

    /**
     * Prepare actions.
     *
     * @return void
     */
    public function prepareActions()
    {
        $this->addAction([
            'title'  => 'View',
            'method' => 'GET',
            'route'  => 'admin.peyd.pending-inventory.view',
            'icon'   => 'eye-icon',
        ]);

        $this->addAction([
            'title'        => 'Delete',
            'method'       => 'DELETE',
            'route'        => 'admin.peyd.pending-inventory.destroy',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'user']),
            'icon'         => 'trash-icon',
        ]);
    }

    /**
     * Prepare mass actions.
     *
     * @return void
     */
    // public function prepareMassActions()
    // {
    //     $this->addMassAction([
    //         'type'   => 'delete',
    //         'label'  => trans('ui::app.datagrid.delete'),
    //         'action' => route('admin.products.mass_delete'),
    //         'method' => 'PUT',
    //     ]);
    // }
}