<?php

namespace MCS\Peyd\Models;

use Illuminate\Database\Eloquent\Model;
use MCS\Peyd\Contracts\PointsMeta as PointsMetaContract;

class PointsMeta extends Model implements PointsMetaContract
{
    protected $fillable = ['type', 'key', 'value'];
    
    protected $table = 'peyd_points_meta';

    // disable timestamp
    public $timestamps = false;

    // update based on type and key
    public static function updateByKey($id, $key, $value)
    {
        return self::where('peyd_point_id', $id)->where('key', $key)->update(['value' => $value]);
    }

    public function point()
    {
        return $this->belongsTo('MCS\Peyd\Models\Points', 'peyd_point_id', 'id');
    }
}