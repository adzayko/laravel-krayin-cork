<?php

namespace MCS\Peyd\Models;

use Illuminate\Database\Eloquent\Model;
use MCS\Peyd\Contracts\Points as PointsContract;

class Points extends Model implements PointsContract
{
    protected $fillable = [
        'type', 
        'total_bought', 
        'buy_rate', 
        'sales_agent', 
        'p_pwp_pwu', 
        'can_use_extra', 
        'account_total', 
        'status', 
        'contact_name'
    ];

    protected $table = 'peyd_points';


    public function meta()
    {
        return $this->hasMany('MCS\Peyd\Models\PointsMeta', 'peyd_point_id', 'id');
    }
}
