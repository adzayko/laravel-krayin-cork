<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // insert columns to peyd_points table
        Schema::table('peyd_points', function (Blueprint $table) {
            $table->string('contact_name')->after('id');
            $table->string('status')->default('pending-inventory')->after('account_total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // remove columns from peyd_points table
        Schema::table('peyd_points', function (Blueprint $table) {
            $table->dropColumn('contact_name');
            $table->dropColumn('status');
        });
    }
};
