<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // update points meta table's column key to unique if peyd_point_id is the same
        Schema::table('peyd_points_meta', function (Blueprint $table) {
            $table->unique(['peyd_point_id', 'key'], 'peyd_points_meta_peyd_point_id_key_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // do the reverse
        Schema::table('peyd_points_meta', function (Blueprint $table) {
            $table->dropUnique('peyd_points_meta_peyd_point_id_key_unique');
        });
    }
};
