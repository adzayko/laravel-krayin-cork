<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peyd_points', function (Blueprint $table) 
        {
            $table->id();
            $table->string('type');
            $table->integer('total_bought')->default(0);
            $table->float('buy_rate', 8, 2)->default(0);
            $table->bigInteger('sales_agent')->nullable();
            $table->string('p_pwp_pwu');
            $table->boolean('can_use_extra')->default(false);
            $table->float('account_total', 8, 2);
            $table->timestamps();
        });

        Schema::create('peyd_points_meta', function (Blueprint $table) {
            $table->foreignId('peyd_point_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('type');
            $table->string('key');
            $table->string('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('peyd_points');
        Schema::dropIfExists('peyd_points_meta');
    }
};