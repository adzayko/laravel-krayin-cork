<?php

return [
    // [
    //     'key' => 'peyd',
    //     'name' => 'Peyd',
    //     'route' => 'admin.peyd.index',
    //     'sort' => 2,
    //     'icon-class' => 'folder-icon',
    // ],
    [
        'key' => 'peyd-inventory',
        'name' => 'Inventory',
        'route' => 'admin.peyd.inventory.index',
        'sort' => 2,
        'icon-class' => 'pencil-icon',
    ]
];