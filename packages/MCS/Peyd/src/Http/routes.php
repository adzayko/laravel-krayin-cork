<?php

Route::group([
        'prefix'        => 'admin/peyd',
        'middleware'    => ['web', 'user']
    ], function () {

        Route::get('/', 'MCS\Peyd\Http\Controllers\PeydController@index')->name('admin.peyd.index');
        Route::get('/inventory', 'MCS\Peyd\Http\Controllers\InventoryController@index')->name('admin.peyd.inventory.index');

        Route::group([
            'prefix'        => 'pending-inventory',
            'middleware'    => ['web', 'user']
        ], function () {
            Route::get('/', 'MCS\Peyd\Http\Controllers\InventoryController@viewPending')->name('admin.peyd.pending-inventory.collection');
            Route::get('/view/{point_id}', 'MCS\Peyd\Http\Controllers\InventoryController@viewPending')->name('admin.peyd.pending-inventory.view');
            Route::get('/create', 'MCS\Peyd\Http\Controllers\InventoryController@storePending')->name('admin.peyd.pending-inventory.create');
            Route::post('/store', 'MCS\Peyd\Http\Controllers\InventoryController@storePending')->name('admin.peyd.pending-inventory.store');
            Route::get('/edit/{id}', 'MCS\Peyd\Http\Controllers\InventoryController@editPending')->name('admin.peyd.pending-inventory.edit');
            Route::post('/update/{id}', 'MCS\Peyd\Http\Controllers\InventoryController@updatePending')->name('admin.peyd.pending-inventory.update');
            Route::get('/destroy/{id}', 'MCS\Peyd\Http\Controllers\InventoryController@destroyPending')->name('admin.peyd.pending-inventory.destroy');
        });

});