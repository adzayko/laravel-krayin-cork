<?php

namespace MCS\Peyd\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use MCS\Peyd\Models\Points;
use MCS\Peyd\Datagrids\PendingInventoryDataGrid;

// ['id', 'type', 'total_bought', 'buy_rate', 'sales_agent', 'p_pwp_pwu, 'can_use_extra', 'account_total', 'created_at', 'updated_at']
class InventoryController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('peyd::inventory.index');
    }

    public function viewPending($point_id = null)
    {
        // dd(app(PendingInventoryDataGrid::class)->toJson());
        if (request()->ajax()) {
            return app(PendingInventoryDataGrid::class)->toJson();
        }

        $points = Points::with('meta')->where('id', $point_id)->first();

        if (!$points) {
            // redirect inventory index with '#pending' hash
            return redirect()
                ->route('admin.peyd.inventory.index')
                ->with('error', 'Pending inventory not found.')
                ->fragment('pending-inventory');
        }

        return view('peyd::inventory.view-pending', [
            'points' => $points,
            'point_id' => $point_id,
        ]);
    }

    public function storePending(Request $request)
    {
        try {
            // check if request is post
            if ($request->isMethod('post')) {
                $post = $request->all();
                $point = Points::create($post);

                foreach ($post['meta'] as $key => $value) {
                    $point->meta()->create([
                        'type' => 'dev',
                        'key' => $key,
                        'value' => $value,
                    ]);
                }

                // redirect to view action with parameter point_id
                return redirect()
                    ->route('admin.peyd.pending-inventory.view', ['point_id' => $point->id])
                    ->with('success', 'Successfully created new pending inventory.');
            }

        } catch (\Exception $e) {
            dd($e->getMessage());
        }

        return view('peyd::inventory.create-pending', [
            'points' => new Points(),
        ]);
    }

    public function editPending($id)
    {
        // Search and edit peyd_points with id
        $point = Points::with('meta')->where('id', $id)->first();

        if (!$point) {
            // redirect inventory index with '#pending' hash
            return redirect()
                ->route('admin.peyd.inventory.index')
                ->with('error', 'Pending inventory not found.');
        }

        return view('peyd::inventory.edit-pending', [
            'point' => $point,
        ]);
    }

}
