<?php

namespace MCS\Peyd\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use MCS\Peyd\Http\Resources\PointsResource;
use MCS\Peyd\Models\Points;
use Illuminate\Http\Request;
use MCS\Peyd\Models\PointsMeta;

class PointsController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $result = false;
        try {
            // $data = [ $points = Points::all(), $points->meta->pluck('value', 'key')];
            $result = PointsResource::collection(Points::with("meta")->get());
            // $points = Points::with('meta')->get();
            // $data = $points->map(function ($point) {
            //     return [
            //         'point' => $point,
            //         'meta' => $point->meta->pluck('value', 'key'),
            //     ];
            // });
            // $result = $data;
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
        return $result;
    }

    // Implement other actions (show, store, update, destroy) here

    // Implement update action here
    public function update(Request $request, $id)
    {
        $result = false;
        try {
            $post = $request->all();
            $point = Points::find($id)->with('meta')->first();

            if ($post['isMeta']) {
                $pointsMeta = PointsMeta::updateByKey($point->id, $post['key'], $post['value']);
            } else {
                $data = [$post['key'] => $post['value']];
                $point->update($data);
            }

            $result = new PointsResource($point->refresh());
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
        return $result;
    }
}
